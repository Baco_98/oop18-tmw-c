﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCSharp.GameController;
using ProjectCSharp.WorldController;

namespace Tests
{
    [TestClass]
    public class TestGameController
    {
        readonly GameController Controller = new GameController(new WorldController());

        [TestMethod]
        public void Init()
        {
            Assert.IsFalse(Controller.IsAlive);
            Assert.IsTrue(Controller.ToString().Equals("GameLoop Offline"));
        }

        [TestMethod]
        public void StartLoop()
        {
            Controller.Start();
            Assert.IsTrue(Controller.IsAlive);
            Assert.IsTrue(Controller.ToString().Equals("GameLoop Running"));

            Thread.Sleep(1000);

            Assert.IsTrue(Controller.IsAlive);

        }

        [TestMethod]
        public void StopLoop()
        {
            Controller.Stop();
            Assert.IsTrue(!Controller.IsAlive);
            Assert.IsTrue(Controller.ToString().Equals("GameLoop Offline"));
        }
    }
}
