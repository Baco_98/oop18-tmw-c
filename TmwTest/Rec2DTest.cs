using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCSharp.Common;

namespace TmwTest
{

    [TestClass]
    public class Rec2DTest
    {
        private readonly Rec2D rec = new Rec2D(new P2d(100, 50), new P2d(200, 100));
        private readonly Rec2D rec2 = new Rec2D(new P2d(300, 200), new P2d(400, 150));
        private readonly Rec2D rec3 = new Rec2D(new P2d(0, 0), new P2d(500, 500));
        private readonly Rec2D rec4 = new Rec2D(new P2d(100, 100), 100, 50);
        private readonly Rec2D rec5 = new Rec2D(new P2d(50, 75), new P2d(150, 150));
        private readonly Rec2D rec6 = new Rec2D(new P2d(300, 100), new P2d(400, 50));

        private readonly P2d point = new P2d(150, 75);
        private readonly P2d point1 = new P2d(300, 100);
        private readonly P2d point2 = new P2d(100, 100);
        private readonly P2d point3 = new P2d(200, 20);

        [TestMethod]
        public void TestIsPointIn()
        {
            Assert.IsTrue(rec.IsPointIn(point));
            Assert.IsFalse(rec.IsPointIn(point1));
            Assert.IsTrue(rec.IsPointIn(point2));
            Assert.IsFalse(rec.IsPointIn(point3));
        }

        [TestMethod]
        public void TestIntersect()
        {
            Assert.IsTrue(rec.Intersects(rec5));
            Assert.IsFalse(rec.Intersects(rec6));
        }

        [TestMethod]
        public void TestGeneric()
        {
            Assert.AreEqual(rec.GetWidth(), rec2.GetWidth());
            Assert.AreEqual(rec.GetHeight(), rec2.GetHeight());
            Assert.AreNotEqual(rec.GetWidth(), rec3.GetWidth());
            Assert.AreNotEqual(rec.GetHeight(), rec3.GetHeight());
            Assert.AreNotEqual(rec, rec4);
        }
    }
}
