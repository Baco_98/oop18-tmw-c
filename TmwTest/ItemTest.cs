﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCSharp.Entities;
using ProjectCSharp.Common;
using ProjectCSharp.Item;
using ProjectCSharp.Inventory;

namespace TmwTest
{
    /// <summary>
    /// Class to test the operation of the items.
    /// </summary>
    [TestClass]
    public class ItemTest
    {
        private IMilkEntity milk = new MilkEntity(new P2d(0, 0), new V2d(0, 0), new Dim2D(1000, 1000));
        private static readonly P2d p = new P2d(0, 0);
        private static readonly Dim2D d = new Dim2D(1000, 1000);

        /// <summary>
        /// Method to test EquipmentType and Equipment.
        /// </summary>
        [TestMethod]
        public void TestEquipment()
        {
            ITemporaryItem chocolate = new Equipment(EquipmentType.Chocolate, p, d);
            ITemporaryItem coffee = new Equipment(EquipmentType.Coffee, p, d);

            Assert.AreEqual(EquipmentType.Chocolate.Name, chocolate.GetName());
            Assert.AreEqual(EquipmentType.Chocolate.Duration, chocolate.GetDuration());
            chocolate.UseItem(milk);
            Assert.AreNotEqual(milk.GetDefaultDamage(), milk.GetDamage());
            coffee.UseItem(milk);
            Assert.AreNotEqual(milk.GetDefaultDamage(), milk.GetDamage());
        }

        /// <summary>
        /// Method to test HealingItemType and HealingItem.
        /// </summary>
        [TestMethod]
        public void TestHealingItem()
        {
            IItem free = new HealingItem(HealingItemType.LactoseFreeMilk, p, d);
            IItem skimmed = new HealingItem(HealingItemType.SkimmedMilk, p, d);
            IItem whole = new HealingItem(HealingItemType.WholeMilk, p, d);

            Assert.AreEqual(HealingItemType.LactoseFreeMilk.Description, free.GetDescription());
            Assert.AreEqual(HealingItemType.SkimmedMilk.Name, skimmed.GetName());
            Assert.AreEqual(HealingItemType.WholeMilk.Points, whole.GetPoints());
            milk.TakeDamage(milk.GetMaxHp() / 2);
            free.UseItem(milk);
            Assert.AreNotEqual(milk.GetMaxHp(), milk.GetCurrentHealth());
            milk = new MilkEntity(new P2d(0, 0), new V2d(0, 0), new Dim2D(1000, 1000));
            milk.TakeDamage(milk.GetMaxHp() - 1);
            skimmed.UseItem(milk);
            Assert.AreNotEqual(milk.GetMaxHp(), milk.GetCurrentHealth());
            milk = new MilkEntity(new P2d(0, 0), new V2d(0, 0), new Dim2D(1000, 1000));
            milk.TakeDamage(milk.GetMaxHp() / 2);
            whole.UseItem(milk);
            Assert.AreEqual(milk.GetMaxHp(), milk.GetCurrentHealth());
        }

    }
}
