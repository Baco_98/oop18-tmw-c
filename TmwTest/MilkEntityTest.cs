﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCSharp.Common;
using ProjectCSharp.Entities;


namespace TmwTest
{
    [TestClass]
    public class MilkEntityTest
    {
        private readonly static Dim2D SIZE = new Dim2D(800, 600);
        private readonly static V2d INITIAL_VELOCITY = new V2d(1, 1);
        private readonly static P2d INITIAL_POSITION = new P2d(0, 0);
        private readonly static P2d ABBRACCIO_POSITION = new P2d(15, 15);
        private readonly static P2d FINAL_POSITION = new P2d(50, 50);
        private const int DAMAGE = 30;
        private const int HEAL = 10;
        private const int HIGH_HEAL = 200;
        private const int HIGH_DAMAGE = 200;

        /// <summary>
        /// This method test if the enemies are created in the right way.
        /// </summary>
        [TestMethod]
        public void CreationTest()
        {
            MilkEntity milk = new MilkEntity(INITIAL_POSITION, INITIAL_VELOCITY, SIZE);
            Assert.IsTrue(milk.IsAlive());
            Assert.IsFalse(milk.IsThisTheEnd());
            Assert.IsFalse(milk.ReadyToShoot());
            Assert.IsTrue(new MilkEntity(ABBRACCIO_POSITION, INITIAL_VELOCITY, SIZE).Intersect(milk));
            milk.Destroy();
            Assert.IsFalse(milk.IsAlive());

        }

        /// <summary>
        /// This method test the movement of the milk.
        /// </summary>
        [TestMethod]
        public void MovementTest()
        {
            MilkEntity milk = new MilkEntity(INITIAL_POSITION, INITIAL_VELOCITY, SIZE);
            double speed = milk.GetDefaultSpeed();

            for (int i = 0; i < 10; i++)
            {
                P2d milkNewPosition = milk.GetCurrentPos() + (milk.GetCurrentVel() * (milk.GetSpeed()));

                milk.Update(milkNewPosition);
            }
            Assert.AreEqual(FINAL_POSITION, milk.GetCurrentPos());

            /*For check if the Update method works even without parameters
            for (int i = 0; i < 10; i++)
            { 
                milk.Update();
            }
            Assert.AreEqual(FINAL_POSITION, milk.GetCurrentPos());

            milk.SetSpeed(speed * 2);
            Assert.AreEqual(speed * 2, milk.GetSpeed());
            milk.SetDefaultSpeed();
            Assert.AreEqual(speed, milk.GetSpeed());
        }

        /// <summary>
        /// This method test if the milk set values correctly.
        /// </summary>
        [TestMethod]
        public void ShootingTest()
        {
            MilkEntity milk = new MilkEntity(INITIAL_POSITION, INITIAL_VELOCITY, SIZE);
            int bulletDamage = milk.GetDefaultDamage();
            int reloadTime = milk.GetDefaultTimeForReload();

            milk.SetDamage(bulletDamage * 2);
            Assert.AreEqual(bulletDamage * 2, milk.GetDamage());
            milk.SetDefaultDamage();
            Assert.AreEqual(bulletDamage, milk.GetDamage());

            milk.SetTimeForReload(reloadTime * 2);
            Assert.AreEqual(reloadTime * 2, milk.GetTimeForReload());
            milk.SetDefaultTimeForReload();
            Assert.AreEqual(reloadTime, milk.GetTimeForReload());
        }

        /// <summary>
        /// This method test if the milk can take damage and heal itself.
        /// </summary>
        [TestMethod]
        public void HpTest()
        {
            MilkEntity milk = new MilkEntity(INITIAL_POSITION, INITIAL_VELOCITY, SIZE);
            int maxHp = milk.GetMaxHp();
            milk.TakeDamage(DAMAGE);
            Assert.AreEqual(maxHp - DAMAGE, milk.GetCurrentHealth());
            int milkHp = milk.GetCurrentHealth();
            milk.Heal(HEAL);
            Assert.AreEqual(milkHp + HEAL, milk.GetCurrentHealth());
            milk.Heal(HIGH_HEAL);
            Assert.AreEqual(maxHp, milk.GetCurrentHealth());
            milk = new MilkEntity(INITIAL_POSITION, INITIAL_VELOCITY, SIZE);
            milk.TakeDamage(HIGH_DAMAGE);
            Assert.AreEqual(0, milk.GetCurrentHealth());
            Assert.IsFalse(milk.IsAlive());
        }
    }
}
