﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjectCSharp.GameWorld;
using ProjectCSharp.Common;
using ProjectCSharp.Entities;

namespace Tests
{
    [TestClass]
    public class TestWorld
    {
        IGameWorld Levelone = new Level(new Rec2D(new P2d(0, 0), new P2d(0, 0)));

        [TestMethod]
        public void Init()
        {
            //World should be initally an empty container
            Assert.IsTrue(Levelone.GetItems().Count == 0);
            Assert.IsTrue(Levelone.GetEnemies().Count == 0);
            Assert.IsTrue(Levelone.GetObstacles().Count == 0);
        }

        public void EnemiesTest()
        {
            //World should manage correctly an enemy

            Assert.IsTrue(Levelone.GetEnemies().Count == 0);

            var enemy = new EnemyAbbraccio(new P2d(100, 100), new V2d(0, 0), new Dim2D(800, 600));
            Levelone.InsertEnemy(enemy);
            Assert.IsTrue(Levelone.GetEnemies().Count == 1);

            Assert.IsTrue(Levelone.GetEnemyPosition(enemy).X == enemy.GetCurrentPos().X && Levelone.GetEnemyPosition(enemy).Y == enemy.GetCurrentPos().Y);
            Assert.IsFalse(Levelone.GetEnemies().GetType().Name.Equals(GameEntityType.ABBRACCIO));

            Levelone.RemoveEnemy(enemy);

            Assert.IsFalse(Levelone.GetEnemies().Contains(enemy));
        }
    }
}
