﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCSharp.Entities;
using ProjectCSharp.Common;
using ProjectCSharp.Item;
using ProjectCSharp.Inventory;

namespace TmwTest
{
    /// <summary>
    /// Class to test the operation of the inventory.
    /// </summary>
    [TestClass]
    public class InventoryTest
    {
        private IMilkEntity milk = new MilkEntity(new P2d(0, 0), new V2d(0, 0), new Dim2D(1000, 1000));
        private static readonly P2d p = new P2d(0, 0);
        private static readonly Dim2D d = new Dim2D(1000, 1000);
        IInventory inv = new Inventory();

        /// <summary>
        /// Method to test Inventory adding and removing item.
        /// </summary>
        [TestMethod]
        public void TestInventory()
        {
            
            inv.AddItem(new HealingItem(HealingItemType.SkimmedMilk, p, d));
            inv.RemoveItem(0);
            inv.AddItem(new HealingItem(HealingItemType.SkimmedMilk, p, d));
            Assert.IsFalse(inv.IsFull());
            inv.AddItem(new Equipment(EquipmentType.Chocolate, p, d));
            inv.AddItem(new HealingItem(HealingItemType.LactoseFreeMilk, p, d));
            inv.RemoveItem(0);
            inv.AddItem(new Equipment(EquipmentType.Coffee, p, d));
            inv.AddItem(new HealingItem(HealingItemType.SkimmedMilk, p, d));
            Assert.IsFalse(inv.IsFull());
            inv.AddItem(new HealingItem(HealingItemType.WholeMilk, p, d));
            Assert.IsTrue(inv.IsFull());
            inv.RemoveItem(0);
            Assert.IsFalse(inv.IsFull());
            inv.AddItem(new Equipment(EquipmentType.Chocolate, p, d));
            Assert.IsTrue(inv.IsFull());
        }
    }
}
