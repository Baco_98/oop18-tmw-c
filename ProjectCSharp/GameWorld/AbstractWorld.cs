﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using ProjectCSharp.Entities;
using ProjectCSharp.Common;
using ProjectCSharp.GameObjects;
using ProjectCSharp.Item;

namespace ProjectCSharp.GameWorld
{
    public enum WorldEvents
    {
        /**
         * Item-pick event.
         */
        ITEMPICK,

        /**
         * Enemy-killed event.
         */
        ENEMY_KILLED,

        /**
         * Bullet-hit event.
         */
        BULLET_HIT,

        /**
         * Shoot event.
         */
        SHOOT,

        /**
         * Obstacle destroy event.
         */
        OBSTACLE_DESTROYED,

        /**
         * Player death event.
         */
        PLAYER_DEATH
    };

    public abstract class AbstractWorld : IGameWorld
    {

        public Rec2D WorldArea { get; set; }
        internal IGameEntity Player { get => Player; set => Player = value; }
        internal P2d PlayerPosition { get => PlayerPosition; set => PlayerPosition = value; }

        readonly List<IItem> worldItems = new List<IItem>();
        readonly List<IGameEntity> enemies = new List<IGameEntity>();
        readonly List<IGameEntity> bullets = new List<IGameEntity>();
        readonly List<IGameObject> obstacles = new List<IGameObject>();
        readonly IObservable<WorldEvents> observable;
        readonly List<IObserver<WorldEvents>> observers = new List<IObserver<WorldEvents>>();

        /// <summary>
        /// Public constructor.
        /// </summary>
        /// <param name="worldArea">world area to set</param>
        public AbstractWorld(Rec2D worldArea)
        {
            WorldArea = worldArea;

            observable = Observable.Empty<WorldEvents>();
        }

        public void InsertItem(IItem obj)
        {
            this.worldItems.Add(obj);
        }

        public void RemoveItem(IItem obj)
        {
            if (this.worldItems.Contains(obj))
            {
                this.worldItems.Remove(obj);
                foreach (var obs in observers)
                {
                    obs.OnNext(WorldEvents.ITEMPICK);
                }
            }
        }

        public void InsertEnemy(IGameEntity enemy)
        {
            if (this.CheckInBounds(enemy))
            {
                this.enemies.Add(enemy);
            }
        }

        public void RemoveEnemy(IGameEntity enemy)
        {
            if (this.enemies.Contains(enemy))
            {
                this.enemies.Remove(enemy);
                foreach (var obs in observers)
                {
                    obs.OnNext(WorldEvents.ENEMY_KILLED);
                }
                ///score
            }
        }

        public void InsertBullet(IGameEntity bullet)
        {
            if (this.CheckInBounds(bullet))
            {
                this.bullets.Add(bullet);
            }
            foreach (var obs in observers)
            {
                obs.OnNext(WorldEvents.SHOOT);
            }
        }

        public void RemoveBullet(IGameEntity bullet)
        {
            if (this.bullets.Contains(bullet))
            {
                this.bullets.Remove(bullet);
            }
        }

        public void InsertPlayer(IGameEntity player)
        {
            if (this.CheckInBounds(player))
            {
                this.Player = player;
                this.PlayerPosition = player.GetCurrentPos();
            }
        }

        public void KillPlayer(IGameEntity player)
        {
            this.Player = null;
            foreach (var obs in observers)
            {
                obs.OnNext(WorldEvents.PLAYER_DEATH);
            }
        }

        public void InsertObstacle(IGameObject obstacle)
        {
            if (this.CheckInBounds(obstacle))
            {
                this.obstacles.Add(obstacle);
            }
        }

        public void RemoveObstacle(IGameObject obstacle)
        {
            if (this.obstacles.Contains(obstacle))
            {
                this.obstacles.Remove(obstacle);
                foreach (var obs in observers)
                {
                    obs.OnNext(WorldEvents.OBSTACLE_DESTROYED);
                }
            }
        }

        public P2d GetEnemyPosition(IGameEntity enemy)
        {
            if (this.enemies.Contains(enemy))
            {
                return enemy.GetCurrentPos();
            }
            return null;
        }

        public P2d GetItemPosition(IItem item)
        {
            if (!this.worldItems.Contains(item))
            {
                return null;
            }
            //return (worldItems.ElementAt(worldItems.IndexOf(item))).GetCurrentPos();
            return new P2d(0, 0);
        }

        public P2d GetPlayerPosition()
        {
            return this.PlayerPosition;
        }

        public ReadOnlyCollection<IItem> GetItems()
        {
            var list = new ReadOnlyCollection<IItem>(worldItems);
            return list;
        }

        public ReadOnlyCollection<IGameEntity> GetEnemies()
        {
            var list = new ReadOnlyCollection<IGameEntity>(enemies);
            return list;
        }

        public  ReadOnlyCollection<IGameObject> GetObstacles()
        {
            var list = new ReadOnlyCollection<IGameObject>(obstacles);
            return list;
        }

        public IGameEntity GetPlayer()
        {
            return this.Player;
        }

        public Rec2D GetWorldArea()
        {
            return this.WorldArea;
        }

        public abstract void SetWorldArea(Rec2D area);
    }
}
