﻿using System.Collections.ObjectModel;

using ProjectCSharp.Entities;
using ProjectCSharp.GameObjects;
using ProjectCSharp.Common;
using ProjectCSharp.Item;

namespace ProjectCSharp.GameWorld
{
    /// <summary>
    /// This class represents a IGameWorld concept.World contains enemies, player,
    /// items and bullets shoot by both enemies and player.All these entities are
    /// closed in a IGameWorldArea which represents the world size.There are multiple
    /// methods which allows to interact with game entities.Allows also to be
    /// observed by observers that'll be aware of changes.
    /// </summary>
    public interface IGameWorld
    {

        ///<section>
        /// Insert item in world. Items have fixed position and must be specified because
        /// items does not have any position value inside their classes.
        /// <param name="obj">obj item to insert</param>
        ///</section>
        void InsertItem(IItem obj);

        ///<section>
        /// Removes item from world.
        /// <param name="obj">obj item to remove</param>
        ///</section>  
        void RemoveItem(IItem obj);

        ///<section>
        /// Insert enemy in world.
        /// <param name="enemy">enemy to insert</param>
        ///</section>
        void InsertEnemy(IGameEntity enemy);


        ///<section>
        /// Removes enemy in world.
        /// <param name="enemy">enemy to remove</param>
        ///</section>
        void RemoveEnemy(IGameEntity enemy);

        ///<section>
        /// Insert bullet in world.
        /// <param name="bullet">bullet to insert</param>
        ///</section>
        void InsertBullet(IGameEntity bullet);

        ///<section>
        /// Removes bullet in world.
        /// <param name="bullet">bullet to remove</param>
        ///</section>
        void RemoveBullet(IGameEntity bullet);

        ///<section>
        /// Insert player in world
        /// <param name="player">player to insert</param>
        ///</section>
        void InsertPlayer(IGameEntity player);


        ///<section>
        /// Removes the player. Represents palyer's death.
        /// <param name="player">player to kill</param>
        ///</section>
        void KillPlayer(IGameEntity player);


        ///<section>
        /// Inserts obstacle in world.
        /// <param name="obstacle">obstacle to insert</param>
        ///</section>
        void InsertObstacle(IGameObject obstacle);

        ///<section>
        /// Removes obstacle in world.
        /// <param name="obstacle">obstacle to remove</param>
        ///</section>
        void RemoveObstacle(IGameObject obstacle);

        ///<section>
        /// Gets enemy position.
        /// <param name="enemy">enemy reference</param>
        ///</section>
        P2d GetEnemyPosition(IGameEntity enemy);


        /// <summary>
        /// Gets item position in world.
        /// </summary>
        /// <param name="item">item to insert</param>
        P2d GetItemPosition(IItem item);

        /// <summary>
        /// Gets player position in world.
        /// </summary>
        P2d GetPlayerPosition();

        /// <summary>
        /// Getter for world items.
        /// </summary>
        ReadOnlyCollection<IItem> GetItems();

        /// <summary>
        /// Getter for enemies in world.
        /// </summary>
        ReadOnlyCollection<IGameEntity> GetEnemies();

        /// <summary>
        /// Getter for all obstacles in world.
        /// </summary>
        ReadOnlyCollection<IGameObject> GetObstacles();

        /// <summary>
        /// Getter for current player in world.
        /// </summary>
        IGameEntity GetPlayer();

        /// <summary>
        /// Returns the worldArea.
        /// </summary>
        Rec2D GetWorldArea();

        /// <summary>
        /// Sets the new world area.
        /// </summary>
        /// <param name="area"> area to set</param>
        void SetWorldArea(Rec2D area);
    }

}
