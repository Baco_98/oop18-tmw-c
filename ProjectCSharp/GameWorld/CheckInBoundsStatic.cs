﻿using ProjectCSharp.Common;
using ProjectCSharp.GameObjects;

namespace ProjectCSharp.GameWorld
{
    static class CheckInBoundsStatic
    {
        public static bool CheckInBounds(this AbstractWorld world, IGameObject obj)
        {

          return (world.GetWorldArea()
                   .IsPointIn(new P2d(obj.GetCurrentPos().X, obj.GetCurrentPos().Y)));
        }
    }
}
