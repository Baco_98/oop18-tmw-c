﻿using ProjectCSharp.Common;
using ProjectCSharp.GameWorld;


namespace ProjectCSharp.GameWorld
{
    public class Level : AbstractWorld
    {
        public Level(Rec2D worldArea) : base(worldArea)
        {
           
        }

        public override void SetWorldArea(Rec2D area)
        {
            WorldArea = area;
        }
    }
}
