﻿using System;

using ProjectCSharp.Entities;

namespace ProjectCSharp
{
    class MilkTimerTest
    {
        public static void Main(string[] args)
        {
            IMilkEntity milk = new MilkEntity(new Common.P2d(0, 0), new Common.V2d(0, 0), new Common.Dim2D(800, 600));

            //Test for the the shooting and the reload time of the milk
            Console.WriteLine("Test for the shooting\n\n");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("Is milk ready to shoot? " + milk.ReadyToShoot() + "\n");
            Console.WriteLine("Milk shoots\n");
            milk.Shoot();
            Console.WriteLine("Is milk ready to shoot? " + milk.ReadyToShoot() + "\n");
            Console.WriteLine("Milk reloads\n");
            System.Threading.Thread.Sleep(500);
            Console.WriteLine("Is milk ready to shoot? " + milk.ReadyToShoot() + "\n\n");

            Console.WriteLine("Press any botton for the next test\n");
            Console.ReadLine();

            //Test for the the taking damage of the milk
            Console.WriteLine("Test for taking the damage\n\n");
            Console.WriteLine("Milk has " + milk.GetCurrentHealth() + " hp\n");
            Console.WriteLine("Milk takes damage\n");
            milk.TakeDamage(50);
            Console.WriteLine("Milk has " + milk.GetCurrentHealth() + " hp\n");
            Console.WriteLine("Milk takes damage, but it's invulnerable so it has the same amount of hp\n");
            milk.TakeDamage(50);
            Console.WriteLine("Milk has " + milk.GetCurrentHealth() + " hp\n");
            System.Threading.Thread.Sleep(600);
            Console.WriteLine("Milk takes damage, now it's vulnerable so it has the less hp\n");
            milk.TakeDamage(50);
            Console.WriteLine("Milk has " + milk.GetCurrentHealth() + " hp\n");

            Console.WriteLine("Press any botton to exit test.\n");
            Console.ReadLine();
        }
    }
}
