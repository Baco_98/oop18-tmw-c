﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCSharp.Entities;
using ProjectCSharp.WorldController;
using System.Collections.ObjectModel;
using ProjectCSharp.Item;

namespace ProjectCSharp.WorldController
{
    public partial class WorldController : IWorldController
    {
        
        int Score;

        public SynchronizedCollection<AbstractGameEntity> EntitiesLoaded { get => EntitiesLoaded; set => EntitiesLoaded = value; }
        internal SynchronizedCollection<IItem> Items { get => Items; set => Items = value; }

        public void RenderEntites()
        {
            //Just renders entites through entities controllers
        }

        public void UpdateEntities(double dt)
        {
            //Updates entites/objects position
        }

        public override string ToString()
        {
            return "World controller beeing invoked.";
        }
    }
}
