﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCSharp
{
    public interface IWorldController
    {


        /// <summary>
        /// Allows to update game entities.
        /// </summary>
        /// <param name="dt"></param>
        void UpdateEntities(double dt);

        /// <summary>
        /// Allows to render entities.
        /// </summary>
        void RenderEntites();
    }
}
