﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCSharp
{
  
    /// <summary>
    /// This class provides a method to be programmed whit a specific behavior.
    /// Typically should be programmed with a strategy that specifies when player can
    /// change room.
    /// For example a simple strategy could be: when all enemies are dead spawn a
    /// door, collision with this door switches to another room.
    /// </summary>

    //Delegate method equivalent to functional interface in java.
    //Just executes the policy.
    public delegate void ExecuteSwitch();

}
