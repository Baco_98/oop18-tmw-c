﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCSharp.GameWorld;

using System.Collections.ObjectModel;

namespace ProjectCSharp.WorldController
{

    /// <summary>
    /// This class represents a tool to populate a gameWorld in order to build <br/>
    /// different levels/gameRooms.It's also the strategy pattern interface.
    /// </summary>
    public partial interface IWorldDispenser
    {

        /// <summary>
        /// Method to insert entities in world.
        /// </summary>
        /// <param name="world">world to populate</param>
        void PopulateWorld(IGameWorld world);

    }
}
