﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCSharp.GameController
{
    /// <summary>
    /// Interface that handles the gameLoop.
    /// </summary>
    public interface IGameController
    {
        /// <summary>
        /// Starts the gameLoop.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops gameLoop.
        /// </summary>
        void Stop();

        /// <summary>
        /// Method that allows to pause gameLoop to reach the correct framerate
        /// </summary>
        /// <param name="current">current time at the beginning of current frame</param>
        void WaitForNextFrame(int current);
    }
}
