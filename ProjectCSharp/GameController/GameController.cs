﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ProjectCSharp.WorldController;

namespace ProjectCSharp.GameController
{
    /// <summary>
    /// Implementation class of IGameController interface.
    /// </summary>
    public class GameController : IGameController
    {

        private readonly IWorldController WorldController;
        private readonly static int PERIOD = 15;
        private readonly static int MILLIS_TO_SEC = 1000;
        private Thread GameLoop;

        public bool IsAlive { get; set; } = false;

        /// <summary>
        /// Public constructor of gameController.
        /// </summary>
        /// <param name="worldController">controller to update on each loop</param>
        public GameController(IWorldController worldController)
        {
            this.WorldController = worldController;
        }

        /// <inheritdoc/>
        public void Start()
        {
            IsAlive = true;
            GameLoop = new Thread(new ThreadStart(Loop));
            GameLoop.Start();
        }

        ///<inheritdoc />
        public void Stop()
        {
            IsAlive = false;
        }

        /// <inheritdoc/>
        public void WaitForNextFrame(int current)
        {
            int dt = Environment.TickCount - current;
            if (dt < PERIOD)
            {
                Thread.Sleep(PERIOD - dt);
            }
        }

        /// <summary>
        /// This method it's the real gameLoop which should be called on a background thread.
        /// It works on WorldController by updating and rendering game objects.
        /// </summary>
        private void Loop()
        {
            long lastMilisTime = Environment.TickCount;
            while (IsAlive)
            {
                int currentMillisTime = Environment.TickCount;
                double elapsedTime = (currentMillisTime - lastMilisTime) / MILLIS_TO_SEC;

                WorldController.ToString();
                WorldController.UpdateEntities(elapsedTime);
                WorldController.RenderEntites();

                WaitForNextFrame(currentMillisTime);
                lastMilisTime = currentMillisTime;
            }
        }

        public override string ToString()
        {
            return IsAlive == false ? "GameLoop Offline" : "GameLoop Running";
        }
    }

}
