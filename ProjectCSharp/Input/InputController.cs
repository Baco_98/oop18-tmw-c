﻿using ProjectCSharp.Common;
using System.Collections.Generic;

namespace ProjectCSharp.Input
{
    class InputController: IInputController
    {


        private const int FIRST_SLOT_INDEX = 0;
        private const int SECOND_SLOT_INDEX = 1;
        private const int THIRD_SLOT_INDEX = 2;
        private const int FOURTH_SLOT_INDEX = 3;
        private const int FIFTH_SLOT_INDEX = 4;
        private int itemPos;
        private V2d vel;
        private readonly List<CharacterState> commandList;

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="view"> view</param>
        public InputController(RoomView view)
        {
            this.commandList = view.GetCommandList;
        }

        /// <inheritdoc/>
        public V2d GetVel => vel;

        /// <inheritdoc/>
        public int GetItemPos => itemPos;

        /// <inheritdoc/>
        public void CleanCommand()
        {
            if (commandList != null)
            {
                this.commandList.Remove(0);
            }
        }

        /// <inheritdoc/>
        public CharacterState Update()
        {
            CharacterState state = commandList == null ? CharacterState.EMPTY_COMMAND : commandList[0];
            if (state != CharacterState.EMPTY_COMMAND)
            {
                this.commandList.Remove(0);
            }
            switch (state)
            {
                case CharacterState.MOVE_UP:
                    this.vel = new V2d(0, -1);
                    return CharacterState.MOVE;
                case CharacterState.MOVE_DOWN:
                    this.vel = new V2d(0, 1);
                    return CharacterState.MOVE;
                case CharacterState.MOVE_LEFT:
                    this.vel = new V2d(-1, 0);
                    return CharacterState.MOVE;
                case CharacterState.MOVE_RIGHT:
                    this.vel = new V2d(1, 0);
                    return CharacterState.MOVE;
                case CharacterState.SHOOT_UP:
                    this.vel = new V2d(0, -1);
                    return CharacterState.SHOOT;
                case CharacterState.SHOOT_DOWN:
                    this.vel = new V2d(0, 1);
                    return CharacterState.SHOOT;
                case CharacterState.SHOOT_LEFT:
                    this.vel = new V2d(-1, 0);
                    return CharacterState.SHOOT;
                case CharacterState.SHOOT_RIGHT:
                    this.vel = new V2d(1, 0);
                    return CharacterState.SHOOT;
                case CharacterState.ITEM1:
                    this.itemPos = FIRST_SLOT_INDEX;
                    return CharacterState.INVENTORY;
                case CharacterState.ITEM2:
                    this.itemPos = SECOND_SLOT_INDEX;
                    return CharacterState.INVENTORY;
                case CharacterState.ITEM3:
                    this.itemPos = THIRD_SLOT_INDEX;
                    return CharacterState.INVENTORY;
                case CharacterState.ITEM4:
                    this.itemPos = FOURTH_SLOT_INDEX;
                    return CharacterState.INVENTORY;
                case CharacterState.ITEM5:
                    this.itemPos = FIFTH_SLOT_INDEX;
                    return CharacterState.INVENTORY;
                default:
                    return CharacterState.EMPTY_COMMAND;
            }
        }
    }
}
