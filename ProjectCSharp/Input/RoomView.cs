﻿using ProjectCSharp.Common;
using System.Collections.Generic;

namespace ProjectCSharp.Input
{
    class RoomView
    {

        private readonly List<CharacterState> commandList = new List<CharacterState>();

        /// <summary>
        /// Getter for command list.
        /// </summary>
        /// <return>list of states represents commands</return>
        public List<CharacterState> GetCommandList => commandList;
    }
}
