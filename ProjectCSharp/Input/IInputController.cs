﻿using ProjectCSharp.Common;

namespace ProjectCSharp.Input
{
    interface IInputController
    {
        /// <summary>
        /// return the main character state. Do not specified the direction or the exact item.
        /// </summary>
        /// <returns>character's state</returns>
        CharacterState Update();

        /// <summary>
        /// Getter for velocity. 
        /// </summary>
        /// <return>velocity of bullet or character</return>
        V2d GetVel { get; }

        /// <summary>
        /// Getter for inventory items position.
        /// </summary>
        /// <return> the item position </return>
        int GetItemPos { get; }

        /// <summary>
        /// Allows to say that last command has been processed.
        /// </summary>
        void CleanCommand();
    }
}
