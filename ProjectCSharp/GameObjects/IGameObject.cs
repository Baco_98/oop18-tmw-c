﻿using System;
using System.Collections.Generic;
using System.Text;

using ProjectCSharp.Common;

namespace ProjectCSharp.GameObjects
{
    public interface IGameObject
    {
        /// <summary>
        /// This method returns the current position of the object.
        /// </summary>
        /// <returns>A P2d which represents the current position of the object</returns>
        P2d GetCurrentPos();

        /// <summary>
        /// This method is used to set a new position to the object, indicated by a P2d.
        /// </summary>
        /// <param name="position">The new position</param>
        void SetPos(P2d position);

        /// <summary>
        /// This method returns the boundary of the object using a Rec2D.
        /// </summary>
        /// <returns>A Rec2D that represents the boundary of the object</returns>
        Rec2D GetBoundary();

        /// <summary>
        /// This method returns the central point of the boundary of the object.
        /// </summary>
        /// <returns>A P2d that is the central position of the object</returns>
        P2d GetCentralPosition();

        /// <summary>
        /// This method returns the dimension of the object.
        /// </summary>
        /// <returns>the dimension of the object</returns>
        Dim2D GetDimension();

        /// <summary>
        /// Setter for the dimension.
        /// </summary>
        /// <param name="dimension">The new dimension</param>
        void SetDimension(Dim2D dimension);

        /// <summary>
        /// This method sets the default dimension using the dimension of the screen.
        /// </summary>
        /// <param name="dimension">The dimension of the screen</param>
        void ResetDefaultDimension(Dim2D dimension);

        /// <summary>
        /// This method is used to check if the object intersects the one passed as parameter.
        /// </summary>
        /// <param name="gameObject">The object to check the intersection with</param>
        /// <returns>True if the two object intersect each other, 
        /// false otherwise</returns>
        bool Intersect(GameObject gameObject);
    }
}
