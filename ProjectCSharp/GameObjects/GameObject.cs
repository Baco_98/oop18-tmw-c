﻿using System;
using System.Collections.Generic;
using System.Text;

using ProjectCSharp.Common;

namespace ProjectCSharp.GameObjects
{
    /// <summary>
    /// This Class is the base for every object in the game and contains information 
    /// about the position of an object and its boundary.This Class cannot be instantiated 
    /// because it represents only a skeleton for the game's object and it's used to make 
    /// easier the implementation of the other objects.
    /// </summary>
    public abstract class GameObject : IGameObject
    {
        private P2d pos;
        private Dim2D dimension;

        /// <summary>
        /// Construct the base for an object from the two base informations,
        /// position and dimension.
        /// </summary>
        /// <param name="pos">The P2d which represents the position of the object</param>
        /// <param name="dimension">The Dim2D which represents the dimensions 
        /// of the object</param>
        protected GameObject(P2d pos, Dim2D dimension)
        {
            this.pos = new P2d(pos.X, pos.Y);
            this.dimension = new Dim2D(dimension.Width, dimension.Height);
        }

        /// <summary>
        /// This method returns the boundary of the object using a Rec2D.
        /// </summary>
        /// <returns>A Rec2D that represents the boundary of the object</returns>
        public Rec2D GetBoundary()
        {
            return new Rec2D(this.pos, this.dimension.Width, this.dimension.Height);
        }

        /// <summary>
        /// This method returns the central point of the boundary of the object.
        /// </summary>
        /// <returns>A P2d that is the central position of the object</returns>
        public P2d GetCentralPosition()
        {
            P2d upperLeftCorner = new P2d(this.GetBoundary().GetMinX(), this.GetBoundary().GetMinY());
            P2d lowerRightCorner = new P2d(this.GetBoundary().GetMaxX(), this.GetBoundary().GetMaxY());
            return new P2d((upperLeftCorner.X + lowerRightCorner.X) / 2,
                    (upperLeftCorner.Y + lowerRightCorner.Y) / 2);
        }

        /// <summary>
        /// This method returns the current position of the object.
        /// </summary>
        /// <returns>A P2d which represents the current position of the object</returns>
        public P2d GetCurrentPos()
        {
            return new P2d(this.pos.X, this.pos.Y);
        }

        /// <summary>
        /// This method returns the dimension of the object.
        /// </summary>
        /// <returns>The dimension of the object</returns>
        public Dim2D GetDimension()
        {
            return new Dim2D(this.dimension.Width, this.dimension.Height);
        }

        /// <summary>
        /// This method is used to check if the object intersects the one passed as parameter.
        /// </summary>
        /// <param name="gameObject">The object to check the intersection with</param>
        /// <returns>True if the two object intersect each other, 
        /// false otherwise</returns>
        public bool Intersect(GameObject gameObject)
        {
            return this.GetBoundary().Intersects(gameObject.GetBoundary());
        }

        /// <summary>
        /// Setter for the dimension.
        /// </summary>
        /// <param name="dimension">The new dimension</param>
        public void SetDimension(Dim2D dimension)
        {
            this.dimension = new Dim2D(dimension.Width, dimension.Height);
        }

        /// <summary>
        /// This method is used to set a new position to the object, indicated by a P2d.
        /// </summary>
        /// <param name="position">The new position</param>
        public void SetPos(P2d position)
        {
            this.pos = new P2d(position.X, position.Y);
        }

        /// <summary>
        /// This method sets the default dimension using the dimension of the screen.
        /// </summary>
        /// <param name="dimension">The dimension of the screen</param>
        public abstract void ResetDefaultDimension(Dim2D dimension);
    }
}
