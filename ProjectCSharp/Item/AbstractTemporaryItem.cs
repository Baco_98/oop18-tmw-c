﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectCSharp.Common;
using System.Threading;

namespace ProjectCSharp.Item
{
    /// <summary>
    /// Abstract class to implements the use of the temporary item
    /// using a thread to cancel the effects once the duration is over.
    /// </summary>
    public abstract class AbstractTemporaryItem : AbstractItem, ITemporaryItem
    {
        private const int SECOND_IN_MILLIS = 1000;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name of the item</param>
        /// <param name="description">Description of the item</param>
        /// <param name="points">Points that give the item</param>
        /// <param name="pos">A P2d which represents the position of the item in the map</param>
        /// <param name="dimension">A Dim2D which represents the dimension of the item</param>
        public AbstractTemporaryItem(string name, string description, int points, P2d pos, Dim2D dimension) : base(name, description, points, pos, dimension)
        {
        }

        /// <summary>
        /// Method to use a specific item implemented by his class.
        /// </summary>
        public sealed override void UseSpecificItem()
        {
            this.ActiveEffect();
            new Thread(Run).Start();
        }

        /// <summary>
        /// Method to wait until the finish of the item effect
        /// </summary>
        private void Run()
        {
            Thread.Sleep(this.GetDuration() * SECOND_IN_MILLIS);
            this.CancelEffect();
        }

        /// <summary>
        /// Method to set the feature to the character.
        /// </summary>
        public abstract void ActiveEffect();

        /// <summary>
        /// Method to stop the effect.
        /// </summary>
        public abstract void CancelEffect();

        /// <summary>
        /// Method to get the duration of the feature (in seconds).
        /// </summary>
        /// <returns></returns>
        public abstract int GetDuration();
    }
}
