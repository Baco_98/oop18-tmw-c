﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectCSharp.Common;

namespace ProjectCSharp.Item
{
    /// <summary>
    /// List of the different type of equipment with their specific characteristics.
    /// </summary>
    public struct EquipmentType
    {
        /// <summary>
        /// Increase the damage by a factor 1.5 for 30 seconds and gives 30 points.
        /// </summary>
        public static readonly EquipmentType Coffee = new EquipmentType("Coffee", "Increase the damage by a factor 1.5", 1.5, 30, 30, 0.03);
        /// <summary>
        /// increase the damage by a factor 2 for 30 seconds and gives 50 points.
        /// </summary>
        public static readonly EquipmentType Chocolate = new EquipmentType("Chocolate", "Increase the damage by a factor 2", 2.0, 30, 50, 0.03);


        public string Name { get; private set; }

        public string Description { get; private set; }

        public double Multiplier { get; private set; }

        public int Duration { get; private set; }

        public int Points { get; private set; }

        public double Proportion { get; private set; }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name of the item</param>
        /// <param name="description">Description of the item</param>
        /// <param name="multiplier">Multiplier of the item</param>
        /// <param name="duration">Duration of the item</param>
        /// <param name="points">Points that the item gives</param>
        /// <param name="proportion">Proportion of the item</param>
        private EquipmentType(string name, string description, double multiplier, int duration, int points, double proportion)
        {
            this.Name = name;
            this.Description = description;
            this.Multiplier = multiplier;
            this.Duration = duration;
            this.Points = points;
            this.Proportion = proportion;
        }
    }

    /// <summary>
    /// A type of item that, when used, increases the damage
    /// inflicted by the character of a percentage for a specific period of time.
    /// </summary>
    public class Equipment : AbstractTemporaryItem
    {
        private readonly EquipmentType type;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="type">The type of the equipment where all his information is contained</param>
        /// <param name="pos">A P2d which represents the position of the item in the map</param>
        /// <param name="fieldSize">A Dim2D which represents the resolution of the game</param>
        public Equipment(EquipmentType type, P2d pos, Dim2D fieldSize)
            : base(type.Name, type.Description, type.Points, pos, new Dim2D(fieldSize.Width * type.Proportion, fieldSize.Width * type.Proportion))
        {
            this.type = type;
        }

        /// <summary>
        /// Method to set the feature to the character.
        /// </summary>
        public override void ActiveEffect()
        {
            this.GetMilk().SetDamage((int) Math.Round(this.GetMilk().GetDefaultDamage() * type.Multiplier));
        }

        /// <summary>
        /// Method to stop the effect.
        /// </summary>
        public override void CancelEffect()
        {
            this.GetMilk().SetDefaultDamage();
        }

        /// <summary>
        /// Method to get the duration of the feature (in seconds).
        /// </summary>
        /// <returns>The duration</returns>
        public override int GetDuration()
        {
            return type.Duration;
        }

        /// <summary>
        /// This method sets a new dimension of the object.
        /// </summary>
        /// <param name="dimension">The dimension of the object</param>
        public override void ResetDefaultDimension(Dim2D dimension)
        {
            this.SetDimension(new Dim2D(type.Proportion * dimension.Width, type.Proportion * dimension.Width));
        }
    }
}
