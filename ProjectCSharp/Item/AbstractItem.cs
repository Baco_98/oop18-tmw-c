﻿using ProjectCSharp.Common;
using ProjectCSharp.Entities;
using ProjectCSharp.GameObjects;

namespace ProjectCSharp.Item
{
    /// <summary>
    /// Abstract class to implements the features and methods common to all types of item.
    /// </summary>
    abstract public class AbstractItem : GameObject, IItem
    {
        private string Name { get; }
        private string Description { get; }
        private int Points { get; }
        private IMilkEntity Milk { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name of the item</param>
        /// <param name="description">Description of the item</param>
        /// <param name="points">Points that give the item</param>
        /// <param name="pos">A P2d which represents the position of the item in the map</param>
        /// <param name="dimension">A Dim2D which represents the dimension of the item</param>
        public AbstractItem(string name, string description, int points, P2d pos, Dim2D dimension) : base(pos, dimension)
        {
            this.Name = name;
            this.Description = description;
            this.Points = points;
        }

        /// <summary>
        /// Getter for the description.
        /// </summary>
        /// <returns>Returns the description of the item</returns>
        public string GetDescription()
        {
            return this.Description;
        }

        /// <summary>
        /// Getter for the name.
        /// </summary>
        /// <returns>Returns the name of the item.</returns>
        public string GetName()
        {
            return this.Name;
        }

        /// <summary>
        /// Getter for the points.
        /// </summary>
        /// <returns>Returns the points to be added to the score
        /// given by the collection of the item.</returns>
        public int GetPoints()
        {
            return this.Points;
        }

        /// <summary>
        /// Method to get the model of the milk.
        /// </summary>
        /// <returns></returns>
        protected IMilkEntity GetMilk()
        {
            return this.Milk;
        }

        /// <summary>
        /// Method to use the item.
        /// </summary>
        /// <param name="milk">The model of the milk character</param>
        public void UseItem(IMilkEntity milk)
        {
            this.Milk = milk;
            this.UseSpecificItem();
        }

        /// <summary>
        /// Method to use a specific item implemented by his class.
        /// </summary>
        public abstract void UseSpecificItem();
    }
}
