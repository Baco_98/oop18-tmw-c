﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectCSharp.Item
{
    /// <summary>
    /// Interface to represent a type of item that, when used,
    /// improves a character's feature for a limited period of time.
    /// </summary>
    public interface ITemporaryItem : IItem
    {
        /// <summary>
        /// Method to set the feature to the character.
        /// </summary>
        void ActiveEffect();

        /// <summary>
        /// Method to stop the effect.
        /// </summary>
        void CancelEffect();

        /// <summary>
        /// Method to get the duration of the feature (in seconds).
        /// </summary>
        /// <returns>The duration</returns>
        int GetDuration();
    }
}
