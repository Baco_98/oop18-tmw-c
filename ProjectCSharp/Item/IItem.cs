﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectCSharp.GameObjects;
using ProjectCSharp.Entities;

namespace ProjectCSharp.Item
{
    /// <summary>
    /// Interface to represent the items obtainable in the map.
    /// The different existing types, if necessary, will extend this interface.
    /// </summary>
    public interface IItem : IGameObject
    {
        /// <summary>
        /// Getter for the name.
        /// </summary>
        /// <returns>Returns the name of the item.</returns>
        string GetName();

        /// <summary>
        /// Getter for the description.
        /// </summary>
        /// <returns>Returns the description of the item</returns>
        string GetDescription();

        /// <summary>
        /// Getter for the points.
        /// </summary>
        /// <returns>Returns the points to be added to the score
        /// given by the collection of the item.</returns>
        int GetPoints();

        /// <summary>
        /// Method to use the item.
        /// </summary>
        /// <param name="milk">The model of the milk character</param>
        void UseItem(IMilkEntity milk);

        /// <summary>
        /// Method to use a specific item implemented by his class.
        /// </summary>
        void UseSpecificItem();
    }
}
