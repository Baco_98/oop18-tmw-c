﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectCSharp.Common;

namespace ProjectCSharp.Item
{
    /// <summary>
    /// List of the different type of consumable with their specific characteristics.
    /// </summary>
    public struct HealingItemType
    {
        /// <summary>
        /// Adds 25% of life to the character and gives 10 points.
        /// </summary>
        public static readonly HealingItemType LactoseFreeMilk = new HealingItemType("Lactose-free milk", "Restores 25% of the life", 0.25, 10, 0.03);
        /// <summary>
        /// Adds 50% of life to the character and gives 20 points.
        /// </summary>
        public static readonly HealingItemType SkimmedMilk = new HealingItemType("Skimmed milk", "Restores 50% of the life", 0.50, 20, 0.03);
        /// <summary>
        /// Adds 100% of life to the character and gives 30 points.
        /// </summary>
        public static readonly HealingItemType WholeMilk = new HealingItemType("Whole milk", "Restores 100% of the life", 1.0, 30, 0.03);


        public string Name { get; private set; }

        public string Description { get; private set; }

        public double Multiplier { get; private set; }

        public int Points { get; private set; }

        public double Proportion { get; private set; }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name of the item</param>
        /// <param name="description">Description of the item</param>
        /// <param name="multiplier">Multiplier of the item</param>
        /// <param name="points">Points that the item gives</param>
        /// <param name="proportion">Proportion of the item</param>
        private HealingItemType(string name, string description, double multiplier, int points, double proportion)
        {
            this.Name = name;
            this.Description = description;
            this.Multiplier = multiplier;
            this.Points = points;
            this.Proportion = proportion;
        }
    }

    /// <summary>
    /// A type of item that, when used, restore a percentage of the character's life.
    /// </summary>
    public class HealingItem : AbstractItem
    {
        private readonly HealingItemType type;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="type">The type of the consumable where all his information is contained</param>
        /// <param name="pos">A P2d which represents the position of the item in the map</param>
        /// <param name="fieldSize">A Dim2D which represents the resolution of the game</param>
        public HealingItem(HealingItemType type, P2d pos, Dim2D fieldSize)
            : base(type.Name, type.Description, type.Points, pos, new Dim2D(fieldSize.Width*type.Proportion, fieldSize.Width * type.Proportion))
        {
            this.type = type;
        }

        /// <summary>
        /// Method to increase character's life when he uses a consumable.
        /// </summary>
        public override void UseSpecificItem()
        {
            this.GetMilk().Heal((int) Math.Round(this.GetMilk().GetMaxHp() * type.Multiplier));
        }

        /// <summary>
        /// This method sets a new dimension of the object.
        /// </summary>
        /// <param name="dimension">The dimension of the object</param>
        public override void ResetDefaultDimension(Dim2D dimension)
        {
            this.SetDimension(new Dim2D(type.Proportion * dimension.Width, type.Proportion * dimension.Width));
        }

    }
}
