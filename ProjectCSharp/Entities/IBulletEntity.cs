﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectCSharp.Entities
{
    public interface IBulletEntity: IGameEntity
    {
        /// <summary>
        /// Returns the damage that the bullet does when hit an entities.
        /// </summary>
        int Damage { get; }
    }
}
