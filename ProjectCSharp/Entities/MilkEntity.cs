﻿using System.Timers;

using ProjectCSharp.Common;

namespace ProjectCSharp.Entities
{
    public class MilkEntity : AbstractGameEntity, IMilkEntity
    {
        private const int DEFAULT_HP_MILK = 200;
        private const int TIME_TO_SHOOT = 200;
        private const int INVULNERABILITY_TIME = 500;
        private const double DIMENSION_PROPORTION_MILK = 0.04;
        private const int STANDARD_SIZE = 800;
        private const int DEFAULT_DAMAGE = 20;
        private const double STANDARD_SPEED_MILK = 5;

        private int ReloadTime { get; set; }
        private int Damage { get; set; }
        private double NormalSpeed { get; set; }
        private bool Invulnerabilty { get; set; }
        private bool ShootReady { get; set; }

        /// <summary>
        /// Construct a new main character.
        /// </summary>
        /// <param name="pos">The initial position of the main character</param>
        /// <param name="vel">The initial velocity of the main character </param>
        /// <param name="fieldSize">The game resolution used to calculate the main 
        /// character's dimension</param>
        public MilkEntity(P2d pos, V2d vel, Dim2D fieldSize) :
            base(GameEntityType.MILK, pos, vel, DEFAULT_HP_MILK,
                 STANDARD_SPEED_MILK * fieldSize.Width / STANDARD_SIZE,
                 new Dim2D(fieldSize.Width * DIMENSION_PROPORTION_MILK,
                           fieldSize.Width * DIMENSION_PROPORTION_MILK))
        {
            this.ReloadTime = TIME_TO_SHOOT;
            this.Damage = DEFAULT_DAMAGE;
            this.NormalSpeed = this.GetSpeed();
            this.Invulnerabilty = false;
            this.ShootReady = false;
            this.StartShooterTimer();
        }
        
        private void CreateTimer(int amount , ElapsedEventHandler Event)
        {
            Timer timer = new Timer(amount);
            timer.Elapsed += Event;
            timer.AutoReset = false;
            timer.Enabled = true;
        }

        private void StartShooterTimer()
        {
            this.CreateTimer(this.ReloadTime, ShootReadyEvent);
        }

        /// <summary>
        /// Event raised when the enemy is ready to shoot.
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        /// <param name="e">The event of the timer</param>
        private void ShootReadyEvent(object sender, ElapsedEventArgs e)
        {
            this.SetReadyToShoot(true);
        }

        private void SetReadyToShoot(bool ready)
        {
            lock (this)
            {
                this.ShootReady = ready;
            }
        }

        private void StartInvulnerabilityTimer()
        {
            this.CreateTimer(INVULNERABILITY_TIME, InvulnerabiltyEvent);
        }

        /// <summary>
        /// Event raised when the enemy is no more invulnerable.
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        /// <param name="e">The event of the timer</param>
        private void InvulnerabiltyEvent(object sender, ElapsedEventArgs e)
        {
            this.SetInvulnerability(false);
        }

        private void SetInvulnerability(bool invulnerability)
        {
            lock (this)
            {
                this.Invulnerabilty = invulnerability;
            }
        }

        /// <summary>
        /// Return true if the entity is ready to shoot.
        /// </summary>
        /// <returns>True if the entity is ready for shooting, false otherwise</returns>
        public override bool ReadyToShoot()
        {
            return this.ShootReady;
        }

        /// <summary>
        /// This method sets the default dimension using the dimension of the screen.
        /// </summary>
        /// <param name="dimension">The dimension of the screen</param>
        public override void ResetDefaultDimension(Dim2D dimension)
        {
            this.ResizeUpdate(dimension, STANDARD_SPEED_MILK, DIMENSION_PROPORTION_MILK);
            this.NormalSpeed = this.GetSpeed();
        }

        /// <summary>
        /// When this method is called the entity shoot and update his information
        /// because of this event.
        /// </summary>
        public override void Shoot()
        {
            this.SetReadyToShoot(false);
            this.StartShooterTimer();
        }

        /// <summary>
        /// This method is used to know if the main character if the game is finished,
        /// meaning if the main character is still alive.
        /// </summary>
        /// <returns>True if the game is ended, false otherwise</returns>
        public bool IsThisTheEnd()
        {
            return !this.IsAlive();
        }

        /// <summary>
        /// Returns the max value of hp.
        /// </summary>
        /// <returns>The max value of hp</returns>
        public int GetMaxHp()
        {
            return DEFAULT_HP_MILK;
        }

        /// <summary>
        /// Heals the main character.
        /// </summary>
        /// <param name="amount">The amount of hp that the main character recover</param>
        public void Heal(int amount)
        {
            if (this.GetCurrentHealth() + amount > DEFAULT_HP_MILK)
            {
                this.SetHp(DEFAULT_HP_MILK);
            }
            else
            {
                this.SetHp(this.GetCurrentHealth() + amount);
            }
        }

        /// <summary>
        /// Returns the default time for reloading.
        /// </summary>
        /// <returns>The default reload time</returns>
        public int GetDefaultTimeForReload()
        {
            return TIME_TO_SHOOT;
        }

        /// <summary>
        /// Set the default reload time.
        /// </summary>
        public void SetDefaultTimeForReload()
        {
            this.ReloadTime = TIME_TO_SHOOT;
        }

        /// <summary>
        /// Returns the actually time for reloading.
        /// </summary>
        /// <returns>The reload time at that moment</returns>
        public int GetTimeForReload()
        {
            return this.ReloadTime;
        }

        /// <summary>
        /// Set the time for reload a new bullet to shoot.
        /// </summary>
        /// <param name="timeForReload">The reload time to be set</param>
        public void SetTimeForReload(int timeForReload)
        {
            this.ReloadTime = timeForReload;
        }

        /// <summary>
        /// Returns the default damage of the bullet shot by the main character.
        /// </summary>
        /// <returns>The default damage of the main character's bullet</returns>
        public int GetDefaultDamage()
        {
            return DEFAULT_DAMAGE;
        }

        /// <summary>
        /// Set the default reload bullet's damage.
        /// </summary>
        public void SetDefaultDamage()
        {
            this.Damage = DEFAULT_DAMAGE;
        }

        /// <summary>
        /// Returns the bullet's damage shot by the main character.
        /// </summary>
        /// <returns>the bullet's damage</returns>
        public int GetDamage()
        {
            return this.Damage;
        }

        /// <summary>
        /// Set the bullet's damage shot by the main character.
        /// </summary>
        /// <param name="newDamage">The bullet's damage to be set</param>
        public void SetDamage(int newDamage)
        {
            this.Damage = newDamage;
        }

        /// <summary>
        /// Override of the base method becuase if the milk is invulnearble it won't take
        /// damage.
        /// </summary>
        /// <param name="damage">The amount of damage</param>
        public override void TakeDamage(int damage)
        {
            if (!this.Invulnerabilty)
            {
                this.SetInvulnerability(true);
                this.StartInvulnerabilityTimer();
                base.TakeDamage(damage);
            }
        }

        /// <summary>
        /// Returns the default speed.
        /// </summary>
        /// <returns>The default speed</returns>
        public double GetDefaultSpeed()
        {
            return this.NormalSpeed;
        }

        /// <summary>
        /// Set the default speed.
        /// </summary>
        public void SetDefaultSpeed()
        {
            this.SetSpeed(this.NormalSpeed);
        }

    }
}
