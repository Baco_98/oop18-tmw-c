﻿using System;
using ProjectCSharp.Common;

namespace ProjectCSharp.Entities
{
    public class BossEntity : AbstractGameEntity, IBossEntity
    {
        private const int DEFAULT_HP_BOSS = 200;
        private const int TIME_TO_SHOOT = 2000;
        private const int TIME_TO_CHARGE = 8000;
        private const double DIMENSION_PROPORTION_BOSS = 0.08;
        private const int STANDARD_SIZE = 800;
        private const double STANDARD_SPEED_BOSS = 2;
        private const int SCORE = 1000;
        private const int CONTACT_DAMAGE = 10;

        private bool charging;
        private double normalSpeed;
        private bool chargeReady;
        private bool shootReady;

        /// <summary>
        /// Public Constructor
        /// </summary>
        /// <param name="pos"> boss position</param>
        /// <param name="vel"> boss velocity</param>
        /// <param name="fieldSize"> fieldsize</param>
        public BossEntity(P2d pos,  V2d vel,  Dim2D fieldSize) : base(GameEntityType.BOSS, pos, vel, DEFAULT_HP_BOSS,
                STANDARD_SPEED_BOSS * fieldSize.Width / STANDARD_SIZE,
                new Dim2D(fieldSize.Width * DIMENSION_PROPORTION_BOSS,
                        fieldSize.Width * DIMENSION_PROPORTION_BOSS))

        {
            this.normalSpeed = base.GetSpeed();
            this.charging = false;
            this.chargeReady = false;
            this.shootReady = false;
            this.StartShooterTimer();
            this.StartChargeTimer();
        }

        private bool ChargeReady
        {
            set { chargeReady = value; }

        }

        private void StartChargeTimer()
        {
            new System.Timers.Timer(BossEntity.TIME_TO_CHARGE);
            ChargeReady = true;
        }

        private bool ShootReady
        {
            set { shootReady = value; }
        }

        private void StartShooterTimer()
        {
            new System.Timers.Timer(BossEntity.TIME_TO_SHOOT);
            ShootReady = true;

        }

        private void SetChargeSpeed()
        {
            base.SetSpeed(this.normalSpeed * 2);
        }

        private void SetNormalSpeed()
        {
            base.SetSpeed(this.normalSpeed);
        }


        private bool IsChargeReady
        {
            get { return chargeReady; }
        }

        /// <inheritdoc/>
        public bool IsUsingSpecialAttack
        {
            get { return charging; }
        }

        /// <inheritdoc/>
        public bool ReadyForSpecialAttack
        {
            get { return IsChargeReady; }
        }

        /// <inheritdoc/>
        public int Score => SCORE;

        /// <inheritdoc/>
        public int ContactDamage => CONTACT_DAMAGE;

        /// <inheritdoc/>
        public override bool ReadyToShoot()
        {
            return shootReady;
        }

        /// <inheritdoc/>
        public override void ResetDefaultDimension(Dim2D dimension)
        {
            this.ResizeUpdate(dimension, STANDARD_SPEED_BOSS, DIMENSION_PROPORTION_BOSS);
            this.normalSpeed = base.GetSpeed();

            if (this.charging)
            {
                base.SetSpeed(normalSpeed * 2);
            }
        }

        /// <inheritdoc/>
        public override void Shoot()
        {
            this.ShootReady = false;
            this.StartShooterTimer();
        }

        /// <inheritdoc/>
        public void StartSpecialAttack()
        {
            this.ChargeReady = false;
            this.charging = true;
            this.SetChargeSpeed();
        }

        /// <inheritdoc/>
        public void StopSpecialAttack()
        {
            this.StartChargeTimer();
            this.charging = false;
            this.SetNormalSpeed();
        }


    }
}
