﻿using System;
using System.Collections.Generic;
using System.Text;

using ProjectCSharp.Common;
using ProjectCSharp.GameObjects;

namespace ProjectCSharp.Entities
{
    public abstract class AbstractGameEntity : GameObject, IGameEntity
    {

        private const int STANDARD_SIZE = 800;

        private int hp;
        private readonly GameEntityType type;
        private V2d vel;
        private double speed;

        /// <summary>
        /// Base Constructor for every entity.
        /// </summary>
        /// <param name="type">The GameEntityType value that identify the entity</param>
        /// <param name="pos">The initial position of the entity</param>
        /// <param name="vel">The initial velocity of the entity</param>
        /// <param name="hp">The health of the entity</param>
        /// <param name="speed">The speed of the entity</param>
        /// <param name="dimension">The dimension of the entity</param>
        public AbstractGameEntity(GameEntityType type, P2d pos, V2d vel, int hp, double speed, Dim2D dimension) : base(pos, dimension)
        {
            this.type = type;
            this.vel = vel;
            this.hp = hp;
            this.speed = speed;
        }

        /// <summary>
        /// This method should be called when the game is resized to resize the entity 
        /// itself.
        /// </summary>
        /// <param name="newDimension">The new size of the game</param>
        /// <param name="speedMultiplier">The multiplier for the speed</param>
        /// <param name="dimensionMultiplier">The multiplier for the dimension</param>
        protected void ResizeUpdate(Dim2D newDimension, double speedMultiplier,
                double dimensionMultiplier)
        {
            this.SetDimension(new Dim2D(dimensionMultiplier * newDimension.Width,
                                        dimensionMultiplier * newDimension.Width));
            this.SetSpeed((speedMultiplier * newDimension.Width) / STANDARD_SIZE);
            this.SetPos(new P2d(this.GetCurrentPos().X * newDimension.Width / STANDARD_SIZE,
                                this.GetCurrentPos().Y * newDimension.Width / STANDARD_SIZE));
        }

        /// <summary>
        /// Calling this method kills the entity.
        /// </summary>
        public void Destroy()
        {
            this.hp = 0;
        }

        /// <summary>
        /// Get the health of the entity in that moment.
        /// </summary>
        /// <returns>The health of the entity</returns>
        public int GetCurrentHealth()
        {
            return this.hp;
        }

        /// <summary>
        /// Returns the velocity of the entity.
        /// </summary>
        /// <returns>A vector that represents the velocity of the entity</returns>
        public V2d GetCurrentVel()
        {
            return new V2d(this.vel.X, this.vel.Y);
        }

        /// <summary>
        /// Get the speed of the entity.
        /// </summary>
        /// <returns>The speed of the entity</returns>
        public double GetSpeed()
        {
            return this.speed;
        }

        /// <summary>
        /// Returns true if the entity is alive.
        /// </summary>
        /// <returns>True if the health of the entity is higher than zero, 
        /// false otherwise</returns>
        public bool IsAlive()
        {
            return this.hp > 0;
        }

        /// <summary>
        /// Set the health of the entity.
        /// </summary>
        /// <param name="hp">The amount to set in the entity</param>
        public void SetHp(int hp)
        {
            this.hp = hp;
        }

        /// <summary>
        /// Set the speed of the entity.
        /// </summary>
        /// <param name="newSpeed">The new speed of the entity</param>
        public void SetSpeed(double newSpeed)
        {
            this.speed = newSpeed;
        }

        /// <summary>
        /// Set the vector which represents the velocity of the entity.
        /// </summary>
        /// <param name="vel">The vector which represents the velocity of the entity</param>
        public void SetVel(V2d vel)
        {
            this.vel = new V2d(vel.X, vel.Y);
        }

        /// <summary>
        /// Reduce the health of the entity.
        /// </summary>
        /// <param name="damage">The amount of damage taken by the entity</param>
        public virtual void TakeDamage(int damage)
        {
            if (this.hp - damage < 0)
            {
                this.hp = 0;
            }
            else
            {
                this.hp -= damage;
            }
        }

        /// <summary>
        /// Update the entity.
        /// </summary>
        /// <param name="newPosition">A P2d which is the new position for the entity</param>
        public virtual void Update(P2d newPosition = null)
        {
            if(newPosition != null)
            {
                this.SetPos(newPosition);
            }
        }

        /// <summary>
        /// Returns a value of GameEntityType that is used to identify the entity.
        /// </summary>
        /// <returns>The GameEntityType of the entity</returns>
        GameEntityType IGameEntity.GetGameEntityType()
        {
            return this.type;
        }

        /// <summary>
        /// Return true if the entity is ready to shoot.
        /// </summary>
        /// <returns>True if the entity is ready for shooting, false otherwise</returns>
        public abstract bool ReadyToShoot();

        /// <summary>
        /// When this method is called the entity shoot and update his information
        /// because of this event.
        /// </summary>
        public abstract void Shoot();
    }
}
