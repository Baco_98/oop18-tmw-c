﻿using System;
using System.Collections.Generic;
using System.Text;

using ProjectCSharp.Common;
using ProjectCSharp.GameObjects;

namespace ProjectCSharp.Entities
{
    public enum GameEntityType
    {
        /// <summary>
        /// This value identify the main character of the game.
        /// </summary>
        MILK,
        /// <summary>
        /// This value represents the enemy Stella.
        /// </summary>
        STELLA,
        /// <summary>
        /// This value represents the enemy Abbraccio.
        /// </summary>
        ABBRACCIO,
        /// <summary>
        /// This value represents an enemy bullet, or generally a bullet that damage the
        /// damage the main chracter.
        /// </summary>
        BULLET,
        /// <summary>
        /// This value represents the the bullets shoot from the main character.
        /// </summary>
        PLAYER_BULLET,
        /// <summary>
        /// This value represents the boss of the game.
        /// </summary>
        BOSS
    }

    public interface IGameEntity : IGameObject
    {

        /// <summary>
        /// Returns a value of GameEntityType that is used to identify the entity.
        /// </summary>
        /// <returns>The GameEntityType of the entity</returns>
        GameEntityType GetGameEntityType();

        /// <summary>
        /// Returns the velocity of the entity.
        /// </summary>
        /// <returns>A vector that represents the velocity of the entity</returns>
        V2d GetCurrentVel();

        /// <summary>
        /// Set the vector which represents the velocity of the entity.
        /// </summary>
        /// <param name="vel">The vector which represents the velocity of the entity</param>
        void SetVel(V2d vel);

        /// <summary>
        /// Get the speed of the entity.
        /// </summary>
        /// <returns>The speed of the entity</returns>
        double GetSpeed();

        /// <summary>
        /// Set the speed of the entity.
        /// </summary>
        /// <param name="newSpeed">The new speed of the entity</param>
        void SetSpeed(double newSpeed);

        /// <summary>
        /// Returns true if the entity is alive.
        /// </summary>
        /// <returns>True if the health of the entity is higher than zero, 
        /// false otherwise</returns>
        bool IsAlive();

        /// <summary>
        /// Get the health of the entity in that moment.
        /// </summary>
        /// <returns>The health of the entity</returns>
        int GetCurrentHealth();

        /// <summary>
        /// Set the health of the entity.
        /// </summary>
        /// <param name="hp">The amount to set in the entity</param>
        void SetHp(int hp);

        /// <summary>
        /// Reduce the health of the entity.
        /// </summary>
        /// <param name="damage">The amount of damage taken by the entity</param>
        void TakeDamage(int damage);

        /// <summary>
        /// Calling this method kills the entity.
        /// </summary>
        void Destroy();

        /// <summary>
        /// Update the entity.
        /// </summary>
        /// <param name="newPosition">A P2d which is the new position for the entity</param>
        void Update(P2d newPosition);

        /// <summary>
        /// When this method is called the entity shoot and update his information
        /// because of this event.
        /// </summary>
        void Shoot();

        /// <summary>
        /// Return true if the entity is ready to shoot.
        /// </summary>
        /// <returns>True if the entity is ready for shooting, false otherwise</returns>
        bool ReadyToShoot();
        
    }
}
