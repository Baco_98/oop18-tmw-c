﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCSharp.Entities
{
    public interface IBossEntity : IEnemy
    {
        /// <summary>
        /// This method is called to know if the boss is ready for charging the main character.
        /// </summary>
        /// <return>true if the boss is ready to charge, false otherwise.</return>
        bool ReadyForSpecialAttack { get; }

        /// <summary>
        /// This method is called to know if the boss is charging the main character.
        /// </summary>
        /// <return>true if the boss is charging, false otherwise</return>
        bool IsUsingSpecialAttack { get; }

        /// <summary>
        /// This method is called when the boss start charging.
        /// </summary>
        void StartSpecialAttack();

        /// <summary>
        /// This method is called when the boss has to stop charging.
        /// </summary>
        void StopSpecialAttack();
    }
}
