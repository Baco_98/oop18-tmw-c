﻿

using ProjectCSharp.Common;

namespace ProjectCSharp.Entities
{
    public class EnemyAbbraccio : AbstractGameEntity, IEnemy
    {

        private const int DEFAULT_HP_ABBRACCIO = 60;
        private const double DIMENSION_PROPORTION_ABBRACCIO = 0.04;
        private const double STANDARD_SPEED_ABBRACCIO = 1.5;
        private const int STANDARD_SIZE = 800;
        private const int SCORE = 200;
        private const int CONTACT_DAMAGE = 10;

        /// <summary>
        /// Public Constructor
        /// </summary>
        /// <param name="pos"> enemy position</param>
        /// <param name="vel"> enemy velocity</param>
        /// <param name="fieldSize"> fieldsize</param>
        public EnemyAbbraccio(P2d pos, V2d vel, Dim2D fieldSize):base(GameEntityType.ABBRACCIO, pos, vel, DEFAULT_HP_ABBRACCIO,
                ((STANDARD_SPEED_ABBRACCIO * fieldSize.Width) / STANDARD_SIZE),
                new Dim2D(fieldSize.Width * DIMENSION_PROPORTION_ABBRACCIO,
                        fieldSize.Width * DIMENSION_PROPORTION_ABBRACCIO))
        {

        }

        ///<inheritdoc/>
        public int Score
        {
            get { return SCORE; }
        }

        ///<inheritdoc/>
        public int ContactDamage
        {
            get { return CONTACT_DAMAGE; }
        }

        ///<inheritdoc/>
        public override bool ReadyToShoot()
        {
            return false;
        }

        ///<inheritdoc/>
        public override void ResetDefaultDimension(Dim2D dimension)
        {
            this.ResizeUpdate(dimension, STANDARD_SPEED_ABBRACCIO, DIMENSION_PROPORTION_ABBRACCIO);
        }

        ///<inheritdoc/>
        public override void Shoot()
        {
            throw new System.NotImplementedException();
        }
    }
}
