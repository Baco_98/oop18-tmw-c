﻿using System;
using ProjectCSharp.Common;

namespace ProjectCSharp.Entities
{
    public class BulletEntity : AbstractGameEntity, IBulletEntity
    {

        private const int DEFAULT_LIFE_BULLET = 1;
        private const double DIMENSION_PROPORTIONS_BULLET = 0.03;
        private const int STANDARD_SIZE = 800;
        private const int DEFAULT_DAMAGE = 20;
        private const double STANDARD_SPEED_BULLET = 5;

        private int damage;


        /// <summary>
        /// Public Constructor
        /// </summary>
        /// <param name="type"> entity type</param>
        /// <param name="pos"> entity position</param>
        /// <param name="vel"> entity velocity</param>
        /// <param name="fieldSize">fieldsize</param>
        /// <param name="damage"> entity damage</param>
        public BulletEntity(GameEntityType type,  P2d pos,  V2d vel,  Dim2D fieldSize,
            int damage):base(type,
                new P2d(pos.X - ((fieldSize.Width * DIMENSION_PROPORTIONS_BULLET) / 2),
                        pos.Y - ((fieldSize.Width * DIMENSION_PROPORTIONS_BULLET) / 2)),
                vel, DEFAULT_LIFE_BULLET, STANDARD_SPEED_BULLET * fieldSize.Width / STANDARD_SIZE,
                new Dim2D(fieldSize.Width * DIMENSION_PROPORTIONS_BULLET,
                        fieldSize.Width * DIMENSION_PROPORTIONS_BULLET))
        {
            this.damage = damage;
        }


        /// <summary>
        /// Public Constructor
        /// </summary>
        /// <param name="pos"> entity position</param>
        /// <param name="vel"> entity velocity</param>
        /// <param name="fieldSize"> fieldsize</param>
        public BulletEntity( P2d pos,  V2d vel,  Dim2D fieldSize):this(GameEntityType.BULLET, pos, vel, fieldSize, DEFAULT_DAMAGE)
        {   
        }

        ///<inheritdoc/>
        public int Damage => damage;

        ///<inheritdoc/>
        public override bool ReadyToShoot()
        {
            return false;
        }

        ///<inheritdoc/>
        public override void ResetDefaultDimension(Dim2D dimension)
        {
            this.ResizeUpdate(dimension, STANDARD_SPEED_BULLET, DIMENSION_PROPORTIONS_BULLET);
        }

        ///<inheritdoc/>
        public override void Shoot()
        {
            throw new NotImplementedException();
        }
    }
}
