﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectCSharp.Entities
{
    public interface IMilkEntity : IGameEntity
    {
        /// <summary>
        /// This method is used to know if the main character if the game is finished,
        /// meaning if the main character is still alive.
        /// </summary>
        /// <returns>True if the game is ended, false otherwise</returns>
        bool IsThisTheEnd();

        /// <summary>
        /// Returns the max value of hp.
        /// </summary>
        /// <returns>The max value of hp</returns>
        int GetMaxHp();

        /// <summary>
        /// Heals the main character.
        /// </summary>
        /// <param name="amount">The amount of hp that the main character recover</param>
        void Heal(int amount);

        /// <summary>
        /// Returns the default time for reloading.
        /// </summary>
        /// <returns>The default reload time</returns>
        int GetDefaultTimeForReload();

        /// <summary>
        /// Set the default reload time.
        /// </summary>
        void SetDefaultTimeForReload();

        /// <summary>
        /// Returns the actually time for reloading.
        /// </summary>
        /// <returns>The reload time at that moment</returns>
        int GetTimeForReload();

        /// <summary>
        /// Set the time for reload a new bullet to shoot.
        /// </summary>
        /// <param name="timeForReload">The reload time to be set</param>
        void SetTimeForReload(int timeForReload);

        /// <summary>
        /// Returns the default damage of the bullet shot by the main character.
        /// </summary>
        /// <returns>The default damage of the main character's bullet</returns>
        int GetDefaultDamage();

        /// <summary>
        /// Set the default reload bullet's damage.
        /// </summary>
        void SetDefaultDamage();

        /// <summary>
        /// Returns the bullet's damage shot by the main character.
        /// </summary>
        /// <returns>the bullet's damage</returns>
        int GetDamage();

        /// <summary>
        /// Set the bullet's damage shot by the main character.
        /// </summary>
        /// <param name="newDamage">The bullet's damage to be set</param>
        void SetDamage(int newDamage);

        /// <summary>
        /// Returns the default speed.
        /// </summary>
        /// <returns>The default speed</returns>
        double GetDefaultSpeed();

        /// <summary>
        /// Set the default speed.
        /// </summary>
        void SetDefaultSpeed();
    }
}
