﻿using ProjectCSharp.Common;

namespace ProjectCSharp.Entities
{
    public class EnemyStella : AbstractGameEntity, IEnemy
    {
        private const int DEFAULT_HP_STELLE = 40;
        private const int TIME_TO_SHOOT = 2000;
        private const double DIMENSION_PROPORTION_STELLE = 0.04;
        private const int STANDARD_SIZE = 800;
        private const int SCORE = 100;
        private const double STANDARD_SPEED_STELLE = 0;
        private const int CONTACT_DAMAGE = 20;

        private bool shootReady;

        /// <summary>
        /// Public Constructor
        /// </summary>
        /// <param name="pos"> enemy position</param>
        /// <param name="vel">enemy velocity</param>
        /// <param name="fieldSize"> fieldsize</param>
        public EnemyStella(P2d pos, V2d vel, Dim2D fieldSize) : base(GameEntityType.STELLA, pos, vel, DEFAULT_HP_STELLE,
                STANDARD_SPEED_STELLE * fieldSize.Width / STANDARD_SIZE,
                new Dim2D(fieldSize.Width * DIMENSION_PROPORTION_STELLE,
                        fieldSize.Width * DIMENSION_PROPORTION_STELLE))
        {
            this.shootReady = false;
            this.StartShooterTimer();
        }

        private bool SetReadyToShoot
        {
            set { shootReady = value; }
        }

        private void StartShooterTimer()
        {
            new System.Timers.Timer(EnemyStella.TIME_TO_SHOOT);
            SetReadyToShoot = true;
        }

        ///<inheritdoc/>
        public int Score => SCORE;

        ///<inheritdoc/>
        public int ContactDamage => CONTACT_DAMAGE;

        ///<inheritdoc/>
        public override bool ReadyToShoot()
        {
            return shootReady;
        }

        ///<inheritdoc/>
        public override void ResetDefaultDimension(Dim2D dimension)
        {
            this.ResizeUpdate(dimension, STANDARD_SPEED_STELLE, DIMENSION_PROPORTION_STELLE);
        }

        ///<inheritdoc/>
        public override void Shoot()
        {
            this.SetReadyToShoot = false;
            this.StartShooterTimer();
        }
    }
}
