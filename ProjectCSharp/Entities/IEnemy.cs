﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectCSharp.Entities
{
    public interface IEnemy
    {
        /// <summary>
        /// This method is used to get the points gained for killing the enemy.
        /// </summary>
        /// <return> the points for killing this enemy</return> 
        int Score { get; }

        /// <summary>
        /// This method returns the damage that the enemy does when touch the main character.
        /// </summary>
        /// <return> the contact damage of this enemy</return>
        int ContactDamage { get; }
    }
}
