﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using ProjectCSharp.Item;

namespace ProjectCSharp.Inventory
{
    /// <summary>
    /// Class to manage the items that the character carries in the inventory
    /// and to add new item collected in the game map.
    /// </summary>
    public class Inventory : IInventory
    {
        private readonly List<IItem> items;
        private const int MAX_SLOT = 5;

        /// <summary>
        /// The constructor of InventoryImpl which initializes empty slot.
        /// </summary>
        public Inventory()
        {
            this.items = new List<IItem>(MAX_SLOT);
            for(int i=0; i < MAX_SLOT; i++)
            {
                items.Add(null);
            }
        }

        /// <summary>
        /// This method check if the inventory is full or not.
        /// </summary>
        /// <returns>Returns true if the inventory is full.</returns>
        public bool IsFull()
        {
            for(int i=0; i < MAX_SLOT; i++)
            {
                if (items[i] == null)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// This method add the passed item in the Inventory.
        /// </summary>
        /// <param name="item">The item to add.</param>
        public void AddItem(IItem item)
        {
            for (int i=0; i < MAX_SLOT; i++)
            {
                if (items[i] == null)
                {
                    items[i] = item;
                    return;
                }
            }
        }

        /// <summary>
        /// This method remove an item from the Inventory.
        /// </summary>
        /// <param name="index">The index of the item to be removed.</param>
        public void RemoveItem(int index) => items[index] = null;

        /// <summary>
        /// This method return the item in a specific position of the inventory.
        /// It will be called when the user decides to use that item.
        /// <para></para>
        /// If there is no item in this position a null will be returned
        /// </summary>
        /// <param name="index">The index of the item</param>
        /// <returns>Returns an item or a null</returns>
        public IItem GetItem(int index)
        {
            return index < MAX_SLOT ? items[index] : null;
        }

        /// <summary>
        /// Returns a list of the inventory's items.
        /// <para></para>
        /// If no object is present in a position, the element of the List will contain a null.
        /// </summary>
        /// <returns>Returns the List of items in the inventory.</returns>
        public List<IItem> GetAll() => items;
    }
}
