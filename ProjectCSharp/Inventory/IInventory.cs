﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectCSharp.Item;

namespace ProjectCSharp.Inventory
{
    /// <summary>
    /// Interface to represent the character's inventory where the items
    /// he collects on the map are placed.
    /// </summary>
    public interface IInventory
    {
        /// <summary>
        /// This method check if the inventory is full or not.
        /// </summary>
        /// <returns>Returns true if the inventory is full.</returns>
        bool IsFull();

        /// <summary>
        /// This method add the passed item in the Inventory.
        /// </summary>
        /// <param name="item">The item to add.</param>
        void AddItem(IItem item);

        /// <summary>
        /// This method remove an item from the Inventory.
        /// </summary>
        /// <param name="index">The index of the item to be removed.</param>
        void RemoveItem(int index);

        /// <summary>
        /// This method return the item in a specific position of the inventory.
        /// It will be called when the user decides to use that item.
        /// <para></para>
        /// If there is no item in this position a null will be returned
        /// </summary>
        /// <param name="index">The index of the item</param>
        /// <returns>Returns an item or a null</returns>
        IItem GetItem(int index);

        /// <summary>
        /// Returns a list of the inventory's items.
        /// <para></para>
        /// If no object is present in a position, the element of the List will contain a null.
        /// </summary>
        /// <returns>Returns the List of items in the inventory.</returns>
        List<IItem> GetAll();
    }
}
