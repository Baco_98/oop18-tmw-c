﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCSharp.Common
{
    /// <summary>
    /// The P2d class defines a point representing a location in (x,y) coordinate space.
    /// </summary>
    public class P2d
    {
        public double X { get; }
        public double Y { get; }

        /// <summary>
        /// Construc a new P2d.
        /// </summary>
        /// <param name="x">the x coordinate</param>
        /// <param name="y">the y coordinate</param>
        public P2d(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Override of the + operator
        /// </summary>
        /// <param name="point">The point for the sum</param>
        /// <param name="v">The vector for the sum</param>
        /// <returns>A new P2d that is the point moved trough the vector</returns>
        public static P2d operator +(P2d point, V2d v)
        {
            return new P2d(point.X + v.X, point.Y + v.Y);
        }

        /// <summary>
        /// Returns a {@link V2d} representing the vector between this point and the one
        /// passed.
        /// </summary>
        /// <param name="p">the P2d representing the vector's tip to compute</param>
        /// <returns>the V2d representing the vector between this point and the
        /// one passed</returns>
        public V2d Sub(P2d p)
        {
            return new V2d(this.X - p.X, this.Y - p.Y);
        }

        /// <summary>
        /// Check if this point and one passed are equals.
        /// </summary>
        /// <param name="obj">the point to be checked</param>
        /// <returns>true if the two points are equals, false otherwise</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (this.GetType() != obj.GetType())
            {
                return false;
            }
            P2d other = (P2d)obj;
            return (!(this.X != other.X || this.Y != other.Y));
        }

        /// <summary>
        /// Get the hash code
        /// </summary>
        /// <returns>the hash code</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}
