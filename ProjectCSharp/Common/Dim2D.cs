﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCSharp.Common
{
    /// <summary>
    /// This class represents a generic 2D dimension.
    /// </summary>
    public class Dim2D
    {
        public double Width { get; set; }
        public double Height { get; set; }

        /// <summary>
        ///  Construct a new dimension.
        /// </summary>
        /// <param name="width">The width of the dimension</param>
        /// <param name="height">The height of the dimension</param>
        public Dim2D(double width, double height)
        {
            this.Width = width;
            this.Height = height;
        }

        /// <summary>
        /// Check if this dimension and the Object passed are equals.
        /// </summary>
        /// <param name="obj">The object passed</param>
        /// <returns>True if the two points are equals, false otherwise</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (this.GetType() != obj.GetType())
            {
                return false;
            }
            Dim2D other = (Dim2D)obj;
            return this.Width == other.Height && this.Height == other.Height;
        }

        /// <summary>
        /// Get the hash code
        /// </summary>
        /// <returns>The hash code</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
