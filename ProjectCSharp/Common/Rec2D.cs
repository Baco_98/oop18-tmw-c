﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCSharp.Common
{
    /// <summary>
    /// This class represents a generic 2D rectangle, it's used for the boundary of the 
    /// objects in the game.
    /// </summary>
    public class Rec2D
    {
        readonly private Dim2D dimension;

        public P2d UpperLeftPoint { get; }
        public P2d LowerLeftPoint { get; }
        public P2d UpperRightPoint { get; }
        public P2d LowerRightPoint { get; }

        /// <summary>
        /// Construct a new rectangle from the upper left and the lower points.
        /// </summary>
        /// <param name="upperLeftPoint">The upper left point of the rectangle</param>
        /// <param name="lowerRightPoint">The lower right poit of the rectangle</param>
        public Rec2D(P2d upperLeftPoint, P2d lowerRightPoint)
        {
            UpperLeftPoint = upperLeftPoint;
            LowerRightPoint = lowerRightPoint;
            this.dimension = new Dim2D(Math.Abs(this.LowerRightPoint.X - this.UpperLeftPoint.X),
                Math.Abs(this.LowerRightPoint.Y - this.UpperLeftPoint.Y));
            this.UpperRightPoint = new P2d(this.LowerRightPoint.X, this.UpperLeftPoint.Y);
            this.LowerLeftPoint = new P2d(this.UpperLeftPoint.X, this.LowerRightPoint.Y);
        }

        /// <summary>
        /// Construct a new rectangle from the upper left point, the width and the height.
        /// </summary>
        /// <param name="upperLeftPoint">The upper left point of the rectangle</param>
        /// <param name="width">The width of the reactangle</param>
        /// <param name="height">The height of the rectangle</param>
        public Rec2D(P2d upperLeftPoint, double width, double height) : 
            this(upperLeftPoint, 
                new P2d(upperLeftPoint.X + width, upperLeftPoint.Y + height))
        {

        }

        /// <summary>
        /// Return the x coordinate of the upper left point.
        /// </summary>
        /// <returns>The x coordinate of the upper left point</returns>
        public double GetMinX()
        {
            return this.UpperLeftPoint.X;
        }

        /// <summary>
        /// Return the y coordinate of the upper left point.
        /// </summary>
        /// <returns>The y coordinate of the upper left point</returns>
        public double GetMinY()
        {
            return this.UpperLeftPoint.Y;
        }

        /// <summary>
        /// Return the x coordinate of the lower right point.
        /// </summary>
        /// <returns>The x coordinate of the lower right point</returns>
        public double GetMaxX()
        {
            return this.LowerRightPoint.X;
        }

        /// <summary>
        /// Return the y coordinate of the lower right point.
        /// </summary>
        /// <returns>The y coordinate of the lower right point</returns>
        public double GetMaxY()
        {
            return this.LowerRightPoint.Y;
        }

        /// <summary>
        /// This method is used to check if a pint is inside the rectangle.
        /// </summary>
        /// <param name="point">The point to check</param>
        /// <returns>True if point is inside the rectangle, false otherwise</returns>
        public bool IsPointIn(P2d point)
        {
            return (point.X >= this.UpperLeftPoint.X && point.X <= UpperRightPoint.X
                    && point.Y >= this.UpperLeftPoint.Y && point.Y <= this.LowerLeftPoint.Y);
        }

        /// <summary>
        /// Return the width of this rectangle.
        /// </summary>
        /// <returns>The width of the rectangle</returns>
        public double GetWidth()
        {
            return this.dimension.Width;
        }

        /// <summary>
        /// Return the height of this rectangle.
        /// </summary>
        /// <returns>The height of the rectangle</returns>
        public double GetHeight()
        {
            return this.dimension.Height;
        }

        /// <summary>
        /// This method is used to check if a rectangle collides with this one.
        /// </summary>
        /// <param name="rectangle">The rectangle to check</param>
        /// <returns>True if the rectangle is colliding with this one, false otherwise</returns>
        public bool Intersects(Rec2D rectangle)
        {
            return ((this.IsPointIn(rectangle.UpperLeftPoint) || this.IsPointIn(rectangle.UpperRightPoint)
                    || this.IsPointIn(rectangle.LowerRightPoint) || this.IsPointIn(rectangle.LowerLeftPoint))
                    || (rectangle.IsPointIn(this.UpperLeftPoint) || rectangle.IsPointIn(this.UpperRightPoint)
                            || rectangle.IsPointIn(LowerRightPoint) || rectangle.IsPointIn(LowerLeftPoint)));
        }

        /// <summary>
        /// Check if this rectangle and the Object passed are equals.
        /// </summary>
        /// <param name="obj">The object passed</param>
        /// <returns>True if the two rectangle are equals, false otherwise</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (this.GetType() != obj.GetType())
            {
                return false;
            }
            Rec2D other = (Rec2D)obj;
            return (this.UpperLeftPoint.Equals(other.UpperLeftPoint) && this.UpperRightPoint.Equals(other.UpperRightPoint)
                    && this.LowerLeftPoint.Equals(other.LowerLeftPoint)
                    && this.LowerRightPoint.Equals(other.LowerRightPoint));
        }

        /// <summary>
        /// Get the hash code
        /// </summary>
        /// <returns>The hash code</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
