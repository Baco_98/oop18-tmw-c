﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCSharp.Common
{
    /// <summary>
    /// This class describes a vector that represents the movement direction for an entity.
    /// </summary>
    public class V2d
    {
        public double X { get; }
        public double Y { get; }

        /// <summary>
        /// Construct a new V2d from its tip's coordinates.
        /// </summary>
        /// <param name="x">The vector's tip x coordinate</param>
        /// <param name="y">The vector's tip y coordinate</param>
        public V2d(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Construct a V2d that link the two P2d passed as parameters.
        /// </summary>
        /// <param name="from">The P2d that represents the start of the vector</param>
        /// <param name="to">The P2d that represents the start of the vector</param>
        public V2d(P2d from, P2d to) : this(to.X  - from.X, to.Y - from.Y)
        {

        }

        /// <summary>
        /// Override of the + operator
        /// </summary>
        /// <param name="v1">The first vector for the sum</param>
        /// <param name="v2">The second vector for the sum</param>
        /// <returns>A new V2d that is the sum of the two vectors</returns>
        public static V2d operator +(V2d v1, V2d v2)
        {
            return new V2d(v1.X + v2.X, v1.Y + v2.Y);
        }

        /// <summary>
        /// Override of the * operator
        /// </summary>
        /// <param name="v">The vector that has to be multiplied</param>
        /// <param name="factor">The factor of the multiplication</param>
        /// <returns>A V2d which is this V2d multiplied for the parameter</returns>
        public static V2d operator *(V2d v, double factor)
        {
            return new V2d(v.X * factor, v.Y * factor);
        }

        /// <summary>
        /// Returns the module of this vector in double precision.
        /// </summary>
        /// <returns>the module of this vector</returns>
        public double Module()
        {
            return (double)Math.Sqrt(this.X * this.X + this.Y * this.Y);
        }

        /// <summary>
        /// Returns a new V2d which is the normalized for of this V2d.
        /// </summary>
        /// <returns>this V2d normalized</returns>
        public V2d GetNormalized()
        {
            double module = this.Module();
            return new V2d(this.X / module, this.Y / module);
        }

        /// <summary>
        /// Check if this V2d and the Object passed are equals.
        /// </summary>
        /// <param name="obj">The object passed</param>
        /// <returns>True if the two V2d are equals, false otherwise</returns>
        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (this.GetType() != obj.GetType())
            {
                return false;
            }
            V2d other = (V2d)obj;
            return (!(this.X != other.X || this.Y != other.Y));
        }

        /// <summary>
        /// Get the hash code
        /// </summary>
        /// <returns>The hash code</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
