﻿
namespace ProjectCSharp.Common
{
    /// <summary>
    /// Enumeration that handle all type of input relative to the player.
    /// </summary>
    public enum CharacterState
    {
        MOVE_UP, MOVE_DOWN, MOVE_LEFT, MOVE_RIGHT, SHOOT_UP, SHOOT_DOWN, SHOOT_LEFT, SHOOT_RIGHT, ITEM1, ITEM2,

        ITEM3, ITEM4, ITEM5, MOVE, SHOOT, INVENTORY, EMPTY_COMMAND
    };

    }
