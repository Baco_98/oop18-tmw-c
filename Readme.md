Michele Bachetti:

Le classi da me implemenate sono divise nei seguenti namespace:  

Common: P2d, V2d, Dim2D, Rec2D;  

GameObjects: IGameObject, GameObject;  

Entities: AbstractGameEntity, IGameEntity, IMilkEntity, MilkEntity;  


Per i test di MIlkEntity ho anche aggiunto una classe dotata di main in modo da verificare il funzionamento dei timer.  


Java e C# sono entarmbi linguaggi object oriented per cui hanno molti aspetti in comune, ma durante lo sviluppo delle mie classi in C# ho potuto notare alcune piccole 
differenze che hanno semplificato o complicato la scrittura delle classi. 
Sicuramente uno degli aspetti più interessanti di C# sono le propietà, in particolare quelle automatiche, che permettono di velocizzare di molto la scrittura di una classe. 
Inoltre durante urante lo sviluppo delle classi P2d e V2d ho potuto approfittare della possibilità di effettuare l'override degli operatori per implementare comportamenti che in Java 
erano definibili solo attraverso altri metodi. 
Una mancanza di C# sono gli Optional, per cui nel contesto in cui li avevo utilizzati in Java li ho sostituiti aggiungendo un valore di default al metodo, cosa non possibile in Java, 
e quindi dare la possibilità di richiamare quel metodo senza passare argomenti. Anche se non rappresentano esattamente la stessa cosa, mi è sembrata la soluzione più ragionevole, 
anche perchè i Nullable offerti da C# sonpo utilizzabili solo con i tipi primitivi.
In conclusione i due linguaggi presentano molte più somiglianze che discrepanze, quindi è stato abbastanza facile passare da uno all'altro.


Nicola Arpino:

Realizzato le classi nei package: GameController,GameWorld e WorldController.

Le principali differenze che ho riscontrato nella trasposizione tra Java e C# sono in primis la mancanza 
degli Optional; i C# sono presenti i Nullable Types ma solo per tipi primitivi. Le interfacce funzionali 
sono realizzabili tramite Delegate mentre non è possibile realizzare metodi di default nelle interfacce.
Per quest'ultimo problema, C# mette a disposizione gli Extension Methods. Ho trovato utile l'utilizzo di
"var" (funzionalità non presente in Java 8) mentre ho riscontrato l'impossibilità  di utilizzare WildCard. 
In generale ho trovato facile la trasposizione in quanto i linguaggi condividono la maggior parte della sintassi. Qualche nota negativa l'ho 
riscontrata nell'uso delle librerie che spesso sono da includere scaricandone di apposite mentre in Java sono
disponibili librerie di sistema per ritornare ,ad esempio, una copia difensiva di una collezione sfruttando
classi con metodi statici.


Alessandra Morellini:

Ho implementato le classi presenti nel package input, EnemyStella EnemyAbbraccio, BossEntity, Bullet Entity e le relative interfacce all'interno del package Entities e la classe CharacterState nel package Common.

Scrivendo in C# una quantità  di classi modesta le differenze che ho notato con java non sono sostanziali.
Le principali difficoltà riscontrate sono dovute alle diverse convenzioni del linguaggio C# e nel diverso utilizzo di keyword come per esempio final che in C# non possiede alcun significato sintattico. 
Le prime ore di lavoro sono risultate meno efficienti poichè è stato necessario assicurarsi di scrivere metodi, classi ed interfacce nel modo corretto. 
Ritengo però che conoscendo java sia possibile affacciarsi allo studio di questo linguaggio con più facilità.


Simone Luzi:

Ho implementato le classi contenute nei package Inventory ed Item.

Le principali differenze che ho riscontrato nella traduzione delle classi Java in C# sono:
-I cambi di keyword come readonly e sealed.  

-L'utilizzo della doc in stile html.  

-Le List che implementano sia le LinkedList che le ArrayList di Java, molto comode in quanto permette di utilizzare  

-Le parentesi quadre come se fosse un vettore e di fare un cambio di valore con un semplice assegnamento.  

-L'assenza di una classe corrispondente ad Optional, in quanto i Nullable possono racchiudere solo tipi primitivi.  

-Le proprietà attraverso le quali si generano i getter e i setter.  

-Le enum con meno possibilità rispetto a Java che mi hanno costretto a convertirle in struct.  
